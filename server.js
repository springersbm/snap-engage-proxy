var http = require('http');
var pkg_uri = require("url");
var request_inner = require('request');
var endpoint = process.env.ENDPOINT;

if(endpoint && process.env.PORT && process.env.EMAIL_STUB ){
    var server = http.createServer(function (request_outer, response_outer) {
    	var request_outer_params = pkg_uri.parse(request_outer.url,true).query;
	console.log('incoming params are...',request_outer_params);
    	var email = request_outer_params.email;
    
    	var resolved_url = endpoint.replace(process.env.EMAIL_STUB,email);
    	if(!(endpoint)){
	    response_outer.statusCode = 500;
	    response_outer.end(JSON.stringify({ error: 'Could not determine email from incoming parameters ', params: request_outer_params }));
    	}else if((endpoint == resolved_url)){
    	    response_outer.statusCode = 500;
	    response_outer.end(JSON.stringify({ error: 'Attempt to replace "'+process.env.EMAIL_STUB+'" with "'+email+'" in "'+endpoint+' FAILED' }));
        }else{
	// all okay process request
	    var headers = {};
	    if(process.env.SAP_USER){
	        headers.auth = { 
		    user: process.env.SAP_USER,
 		    password: process.env.SAP_PASS
		}
	    }
	    console.log('proxying request to url = ',resolved_url);
	    request_inner( resolved_url , headers, function(error,response_inner,body){
		if(error){
		    response_outer.statusCode = response_inner.statusCode;
		    response_outer.end(error);
		} else {
		    response_outer.statusCode = response_inner.statusCode;
		    response_outer.end(body);
		}
	    });
    	}
    });
    server.listen(process.env.PORT);
    console.log("Server running at http://127.0.0.1:" + ( process.env.PORT ) + "/");
} else {
	console.error("Failed to start server. Environmental Variables are missing: at least one of",{
		endpoint : process.env.ENDPOINT,
		port: process.env.PORT,
		email_stub : process.env.EMAIL_STUB
	});
}
