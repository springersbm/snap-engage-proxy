FROM node:slim
EXPOSE 8080

#required arguments
ARG ENDPOINT
ARG PORT 8080
ARG EMAIL_STUB '{{email}}'

#make sure npm is installed so that we can get node packages 
RUN apt-get install npm

#add files from this git repo
ADD server.js

#start up the server: uses ARGS above
CMD ["node server.js"]

